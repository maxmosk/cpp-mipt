#ifndef CACHE_HPP_INCLUDED
#define CACHE_HPP_INCLUDED

#include <cstddef>
#include <type_traits>
#include <unordered_map>
#include <vector>


namespace Cache
{
template<typename T, typename KeyT>
class Ideal final
{
    std::size_t size_;
    std::vector<T> cells_{};
    std::unordered_map<KeyT, std::size_t> htable_{};
    
    const std::vector<T> &pages_;
    KeyT (&slowGetPage_)(KeyT key);
    T (&getKey_)(T page);

    bool full() const
    {
        return cells_.size() == size_;
    }

public:
    explicit Ideal(
            std::size_t size,
            KeyT (&slowGetPage)(KeyT key),
            T (&getKey)(T page),
            const std::vector<T> &pages
                ):
            size_{size},
            pages_(pages),
            slowGetPage_(slowGetPage),
            getKey_(getKey)
    {}
            
    void updatePages(KeyT key)
    {
        cells_.push_back(key);
    }

    std::size_t getHits()
    {
        std::size_t npages = pages_.size();
        std::size_t hits = 0;

        for (std::size_t i = 0; i < npages; ++i)
        {
            if (htable_.contains(pages_[i]))
            {
                ++hits;
            }
            else
            {
                std::size_t replaceIndex = 0;
                for (std::size_t j = 0; j < size_; ++j)
                {
                    
                }
            }
        }

        return hits;
    }
};
}

#endif // CACHE_HPP_INCLUDED
