#include "cache.hpp"
#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <vector>


namespace
{
typedef unsigned Page;

typedef unsigned Key;

Page slowGetPage(Key key)
{
    return key;
}

Key getKey(Page page)
{
    return page;
}
}


int main()
{
    std::size_t size = 0;
    std::size_t pages = 0;

    std::cin >> size >> pages;
    assert(std::cin.good());

    std::vector<Page> pages_array{};
    pages_array.reserve(pages);
    for (std::size_t i = 0; i < pages; i++)
    {
        Page page = 0;
        std::cin >> page;
        assert(std::cin.good());
        pages_array.push_back(page);
    }

    Cache::Ideal<Page, Key> cache{size, slowGetPage, getKey, pages_array};
    std::cout << cache.getHits() << std::endl;

    return EXIT_SUCCESS;
}
